const router = require(`express`).Router();
const agenda = require(`./agenda`);
const goal = require(`./goal`);
const todo = require(`./todo`);
const signin = require(`./signin`);
const profile = require(`./profile`);
const register = require(`./register`);
const homepage = require(`./homepage`);
const goaluser = require('./goaluser');

router.use(agenda);
router.use(homepage);
router.use(goal);
router.use(todo);
router.use(signin);
router.use(profile);
router.use(register);
router.use(goaluser);

module.exports = router;
