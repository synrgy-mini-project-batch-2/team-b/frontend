const { default: axios } = require("axios");
const moment = require("moment");
const router = require(`express`).Router();
const baseUrl = "https://fokusin-backend.herokuapp.com/";

router.get("/todo", async (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    // get goal list
    try {
      let data = await axios.get(`${baseUrl}userGoals/show/?user=${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var dataGoals = data.data;
    } catch (error) {
      var dataGoals = null;
    }
    // console.log(dataGoals);
    // get tasklist
    try {
      let data = await axios.get(`${baseUrl}tasklist/show`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var dataTask = data.data.data;
      // console.log(dataTask);
    } catch (error) {
      var dataTask = null;
    }

    res.render(`todolist/index.ejs`, {
      judul: "To Do List",
      css: "todo/todo",
      title: "todo",
      goalList: dataGoals,
      taskList: dataTask,
      id: id,
      moment: moment,
    });
  } else {
    res.redirect(`/signin`);
  }
});

// add task
router.post(`/addTodoList`, async (req, res) => {
  let { id, token } = req.cookies.login_token;
  // console.log(req.body);

  let data = {
    userId: id,
    namaList: req.body.namaTugas,
    detailList: req.body.goal,
    warnaList: req.body.color,
    dateList: req.body.waktu,
    statusList: "true",
  };
  try {
    let post = await axios.post(`${baseUrl}tasklist/add`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    // console.log(post);
    res.status(201).redirect(`/todo`);
  } catch (error) {
    res.send(error);
  }
});

// tunggu endpoint update todolist
router.put(`/updateTodoList`, async (req, res) => {
  let { id, token } = req.cookies.login_token;
  // console.log(req.body);
  let data = {
    userId: id,
    namaList: req.body.namaTugasUpdate,
    detailList: req.body.goalUpdate,
    warnaList: req.body.color,
    dateList: req.body.waktuUpdate,
    statusList: "true",
  };
  let idList = req.body.idList;
  try {
    let put = await axios.put(`${baseUrl}tasklist/${idList}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    // console.log(put);
    res.status(201).redirect(`/todo`);
  } catch (error) {
    res.send(error);
  }
});
module.exports = router;
