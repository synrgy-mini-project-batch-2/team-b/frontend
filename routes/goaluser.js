const { default: axios } = require("axios");
const router = require(`express`).Router();
const baseUrl = "https://fokusin-backend.herokuapp.com/";

//tampilkan list goal yang diambil user
router.get("/goaluser/", async (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    let param = req.params.id;

    // axios.all([
    //     axios.get(`${baseUrl}userGoals/show/?user=${cookiesid}`,
    //     {
    //         headers: {
    //         Authorization: `Bearer ${token}`,
    //         }
    //     }),
    //     axios.get(`${baseUrl}progress/myProgress/?user=${id}`,
    //     {
    //         headers: {
    //         Authorization: `Bearer ${token}`,
    //         }
    //     })
    //   ])
    //   .then(axios.spread((progress, milestone) => {
    //     // do something with both responses
    //     let judulgoal = mileRes.data[0].Goal.namaGoal
    //     res.render(`goaluser/goal.ejs`, {judul : "Diskusi", css:"goal/diskusi", title:"goal", diskusi: dissRes.data.data, judulgoal, id, cookiesid})
    //   }))
    //   .catch((err) => {
    //     res.status(404).send(err)
    // })

    //show user progress
    try {
      let data = await axios.get(`${baseUrl}userGoals/show/?user=${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var dataGoals = data.data;
    } catch (error) {
      var dataGoals = null;
    }
    if (dataGoals != null) {
      var dataProgress = [];
      await dataGoals.forEach((element, index) => {
        dataProgress[index] = {
          userId: element.userId,
          goalsId: element.goalsId,
          namaGoal: element.Goal.namaGoal,
        };
      });

      var dataProgressBar = [];
      // data progress bar
      for (let index = 0; index < dataProgress.length; index++) {
        try {
          // https://fokusin-backend.herokuapp.com/progress/myProgress?user=J1rb00zL6-WIjWY_jJ52P&goal=ocUA56RvvcUZfbCyKTxM-
          let data = await axios.get(
            `${baseUrl}progress/myProgress?user=${dataProgress[index].userId}&goal=${dataProgress[index].goalsId}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          );
          let progress = eval((data.data.done / data.data.total) * 100);
          dataProgressBar[index] = {
            userId: dataProgress[index].userId,
            goalsId: dataProgress[index].goalsId,
            namaGoal: dataProgress[index].namaGoal,
            data: data.data,
            progress: progress,
          };

          // console.log(data);
        } catch (error) {
          dataProgressBar[index] = {
            userId: dataProgress[index].userId,
            goalsId: dataProgress[index].goalsId,
            namaGoal: dataProgress[index].namaGoal,
            data: "",
          };
          // console.log(error);
        }
      }
    } else {
      var dataProgressBar = {
        userId: null,
        goalsId: null,
        namaGoal: null,
        data: "",
      };
    }
    // console.log(dataProgressBar);
    res.render(`goaluser/goal.ejs`, {
      judul: "Goal",
      css: "goal/goal",
      title: "goal",
      dataProgressBar: dataProgressBar,
    });
  }
});

//list milestone dari user progress milestone
router.get("/goaluser/milestone/detail/:id", async (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    let param = req.params.id;
    console.log(param);
    // console.log(id);
    try {
      let dataProgress = await axios.get(
        `${baseUrl}progress/all/?user=${id}&goal=${param}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      var milestone = dataProgress.data;
      // console.log(dataProgress);
    } catch (error) {
      var milestone = null;
    }

    try {
      let data = await axios.get(`${baseUrl}goals/${param}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      var goal = data.data;
    } catch (error) {
      var goal = null;
    }
    res.render(`goaluser/milestone.ejs`, {
      judul: "Milestone",
      css: "goal/milestone",
      title: "goal",
      milestone: milestone,
      param,
      id,
      goal: goal,
    });
    // axios
    //   .all([
    //     axios.get(`${baseUrl}progress/all/?user=${id}&goal=${param}`, {
    //       headers: {
    //         Authorization: `Bearer ${token}`,
    //       },
    //     }),
    //     axios.get(`${baseUrl}goals/show/?goal=${param}`, {
    //       headers: {
    //         Authorization: `Bearer ${token}`,
    //       },
    //     }),
    //   ])
    //   .then(
    //     axios.spread((mileRes, goalRes) => {
    //       console.log(milRes);
    //       // do something with both responses
    //       // res.render(`goaluser/milestone.ejs`, {
    //       //   judul: "Milestone",
    //       //   css: "goal/milestone",
    //       //   title: "goal",
    //       //   milestone: mileRes.data,
    //       //   param,
    //       //   id,
    //       //   goal: goalRes.data,
    //       // });
    //     })
    //   )
    //   .catch((err) => {
    //     // res.send(err);
    //     // res.render(`goaluser/milestone.ejs`, {
    //     //   judul: "Milestone",
    //     //   css: "goal/milestone",
    //     //   title: "goal",
    //     //   milestone: null,
    //     //   param,
    //     //   id,
    //     //   goal: null,
    //     // });
    //   });
  }
});

router.get("/goaluser/milestone/diskusi/:id", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token, username } = req.cookies.login_token;
    let param = req.params.id;

    axios
      .all([
        axios.get(`${baseUrl}milestone/show/?goal=${param}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }),
        axios.get(`${baseUrl}discussions/show/?goal=${param}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }),
      ])
      .then(
        axios.spread((mileRes, dissRes) => {
          // do something with both responses
          // console.log(mileRes.data)
          // console.log(dissRes.data)
          let judulgoal = mileRes.data[0].Goal.namaGoal;
          let waktu = mileRes.data[0].Goal.estimationTime;
          res.render(`goaluser/diskusi.ejs`, {
            judul: "Diskusi",
            css: "goal/diskusi",
            title: "goal",
            diskusi: dissRes.data.data,
            judulgoal,
            username,
            param,
            id,
            waktu,
          });
        })
      )
      .catch((err) => {
        res.status(404).send(err);
      });
  }
});

router.put("/goaluser/milestone/detail/:id", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    let param = req.params.id;
    let goalid = req.body.goalid;
    console.log(goalid);
    let idku = req.body.id;
    let data = { id: idku, isFinished: req.body.isFinished };
    // console.log(data)
    // console.log(`${baseUrl}progress/?id=${data.id}`)
    axios
      .put(`${baseUrl}progress/${data.id}`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        res.redirect(`${goalid}`);
      })
      .catch((err) => res.status(404).send(err));
  }
});

router.post("/goaluser/diskusi", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + "/" + mm + "/" + dd;
    req.body.diskusiDate = today;
    // console.log(req.body);
    let goalsId = req.body.goalsId;
    // console.log(goalsId);
    axios
      .post(`${baseUrl}discussions/add`, req.body, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        res.redirect(`/goaluser/milestone/diskusi/${goalsId}`);
      })
      .catch((err) => console.log(err));
  }
});

module.exports = router;
