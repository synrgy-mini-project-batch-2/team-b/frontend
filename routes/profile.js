const router = require(`express`).Router();
const { default: axios } = require("axios");
const bcrypt = require(`bcrypt`);
const baseUrl = "https://fokusin-backend.herokuapp.com/";

// profile with parameter for render page in profile feature
router.get("/profile/:param", (req, res) => {
  let param = req.params.param;

  // check if param in profile not signout
  if (param != "signout") {
    if (req.cookies.login_token) {
      if (param == `index`) {
        // get cookie id and token in login_token
        let id = req.cookies.login_token.id;
        let token = req.cookies.login_token.token;
        // get data with axios and receive data from backend user
        axios
          .get(`${baseUrl}user/${id}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            // send data to render profile index
            let data = result.data;
            res.status(201).render(`profile/${param}.ejs`, {
              param: param,
              data: data,
            });
          })
          .catch((err) => {
            console.log(err);
            res.status(403).clearCookie(`login_token`).redirect(`/signin`);
          });
      } else {
        // if param in profile not index
        res.render(`profile/${param}.ejs`, { param: param });
      }
    } else {
      // if no cookies set
      res.redirect(`/signin`);
    }
  }
});

// router to update profile with id for parameter
router.put(`/updateProfile`, (req, res) => {
  let { nama, email, noTelp } = req.body;
  let id = req.cookies.login_token.id;
  let token = req.cookies.login_token.token;
  axios
    .put(
      `${baseUrl}user/${id}`,
      {
        nama: nama,
        email: email,
        noTelp: noTelp,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
    .then((result) => {
      res.status(201).redirect(`/profile/index`);
    })
    .catch((err) => {
      res.send({ message: `Error update`, error: err });
    });
});

router.put(`/updatePassword`, async (req, res) => {
  let { passwordOld, passwordBaru, confirmPassword } = req.body;
  let { id, token } = req.cookies.login_token;
  let dataPassword = await axios.get(`${baseUrl}user/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  // console.log(dataPassword.data);
  let userPassword = dataPassword.data[0].password;
  if (await bcrypt.compare(passwordOld, userPassword)) {
    if (passwordBaru == confirmPassword) {
      await axios.put(
        `${baseUrl}user/${id}`,
        {
          password: passwordBaru,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      console.log(`succes update dengan password ${passwordBaru}`);
      res.status(201).redirect(`/profile/password`);
    } else {
      console.log("Password Baru tidak cocok dengan confirm password");
      res.redirect(`/profile/password`);
    }
  } else {
    console.log("Password lama tidak sesuai");
    res.redirect(`/profile/password`);
  }
});

module.exports = router;
