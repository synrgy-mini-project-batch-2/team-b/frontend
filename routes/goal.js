const { default: axios } = require("axios");
const router = require(`express`).Router();
const baseUrl = "https://fokusin-backend.herokuapp.com/";

router.get("/goal/milestone/detail/:id", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    let param = req.params.id;

    axios
      .all([
        axios.get(`${baseUrl}milestone/show/?goal=${param}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }),
        axios.get(`${baseUrl}userGoals/show/?user=${id}&goal=${param}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }),
      ])
      .then(
        axios.spread((mileRes, userRes) => {
          // do something with both responses
          // console.log(mileRes.data)
          let judulgoal = mileRes.data[0].Goal.namaGoal;
          let waktu = mileRes.data[0].Goal.estimationTime;

          // res.send(mileRes.data);
          res.render(`goal/milestone.ejs`, {
            judul: "Diskusi",
            css: "goal/diskusi",
            title: "goal",
            milestone: mileRes.data,
            idusergoals: userRes.data[0].id,
            judulgoal,
            id,
            waktu,
            param,
          });
        })
      )
      .catch((err) => {
        res.status(404).send(err);
      });
  }
});

router.get("/goal/milestone/diskusi/:id", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token, username } = req.cookies.login_token;
    let param = req.params.id;
    // console.log(param);
    axios
      .all([
        axios.get(`${baseUrl}milestone/show/?goal=${param}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }),
        axios.get(`${baseUrl}discussions/show?goal=${param}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }),
      ])
      .then(
        axios.spread((mileRes, dissRes) => {
          // do something with both responses

          let judulgoal = mileRes.data[0].Goal.namaGoal;
          let waktu = mileRes.data[0].Goal.estimationTime;
          res.render(`goal/diskusi.ejs`, {
            judul: "Diskusi",
            css: "goal/diskusi",
            title: "goal",
            diskusi: dissRes.data.data,
            judulgoal,
            id,
            param,
            waktu,
            username,
          });
        })
      )
      .catch((err) => {
        res.status(404).send(err);
      });
  }
});

router.post("/goal/diskusi", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + "/" + mm + "/" + dd;
    req.body.diskusiDate = today;
    // console.log(req.body);
    let goalsId = req.body.goalsId;
    // console.log(goalsId);
    axios
      .post(`${baseUrl}discussions/add`, req.body, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        res.redirect(`/goal/milestone/diskusi/${goalsId}`);
      })
      .catch((err) => console.log(err));
  }
});

router.post("/goal/adduserprogress", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    // console.log(req.body);
    // console.log(id);
    let goalid = req.body.goalid;
    // console.log(goalid);
    axios
      .post(`${baseUrl}progress/add`, req.body, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        // console.log(result);
        res.redirect(`/goal/milestone/detail/${goalid}`);
      })
      .catch((err) => console.log(err));
  }
});

router.post("/goal/milestone", (req, res) => {
  if (req.cookies.login_token) {
    let { id, token } = req.cookies.login_token;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + "/" + mm + "/" + dd;
    req.body.diskusiDate = today;
    console.log(req.body);
    let goalid = req.body.goalid;
    axios
      .post(`${baseUrl}discussions/add`, req.body, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        res.redirect(`/goal/milestone/diskusi/${goalid}`);
      })
      .catch((err) => console.log(err));
  }
});

module.exports = router;
